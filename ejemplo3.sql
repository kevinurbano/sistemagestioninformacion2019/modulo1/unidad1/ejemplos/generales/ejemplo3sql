﻿DROP DATABASE IF EXISTS b20190531; -- Hoja de Ejercicicios 1  Ejercicio 3
CREATE DATABASE IF NOT EXISTS b20190531;
USE b20190531;

CREATE TABLE profesores(
    dni varchar(9),
    nombre varchar(100),
    direccion varchar(100),
    telefono varchar(14),
    PRIMARY KEY(dni)
  );

CREATE TABLE modulos(
    codigo int AUTO_INCREMENT,
    nombre varchar(100),
    PRIMARY KEY(codigo)
  );

CREATE TABLE alumnos(
    nExpendiente int AUTO_INCREMENT,
    nombre varchar(100),
    apellidos varchar(100),
    fecha_nac date,
    PRIMARY KEY(nExpendiente)
  );

CREATE TABLE delegados(
    delegado int,
    alumno int,
    PRIMARY KEY(delegado,alumno),
    UNIQUE KEY(alumno),
    CONSTRAINT fkExpedienteDelegados FOREIGN KEY(delegado)
      REFERENCES alumnos(nExpendiente) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkExpedienteAlumnos FOREIGN KEY(alumno)
      REFERENCES alumnos(nExpendiente) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE imparten(
    profesor varchar(9),
    modulo int,
    PRIMARY KEY(profesor,modulo),
    UNIQUE KEY(modulo),
    CONSTRAINT fkImpartenProfesores FOREIGN KEY (profesor)
      REFERENCES profesores(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkImpartenModulo FOREIGN KEY(modulo)
      REFERENCES modulos(codigo) ON DELETE CASCADE ON UPDATE CASCADE    
  );

CREATE TABLE matriculado(
    modulo int,
    alumno int,
    PRIMARY KEY(modulo,alumno),
    CONSTRAINT fkMatriculadoModulo FOREIGN KEY (modulo)
      REFERENCES modulos(codigo) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkMatriculadoAlumno FOREIGN KEY (alumno)
      REFERENCES alumnos(nExpendiente) ON DELETE CASCADE ON UPDATE CASCADE  
  );

